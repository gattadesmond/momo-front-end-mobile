function couponwheel_DialogTrigger(e, t) {
    this.complete = !1, this.callback = e, this.timer = null, this.interval = null, this.options = jQuery.extend({
        trigger: "timeout",
        target: "",
        timeout: 0,
        percentDown: 50,
        percentUp: 10,
        scrollInterval: 1e3
    }, t), this.init = function () {
        if ("exitIntent" == this.options.trigger || "exit_intent" == this.options.trigger) {
            var t = this;
            jQuery(document).on("mouseleave", function (e) {
                !t.complete && e.clientY < 0 && (t.callback(), t.complete = !0, jQuery(document).off("mouseleave"))
            })
        } else if ("target" == this.options.trigger) {
            if ("" !== this.options.target)
                if (0 == jQuery(this.options.target).length) this.complete = !0;
                else {
                    var e = jQuery(this.options.target).offset().top;
                    t = this;
                    this.interval = setInterval(function () {
                        jQuery(window).scrollTop() >= e && (clearInterval(t.interval), t.interval = null, t.complete || (t.callback(), t.complete = !0))
                    }, this.options.scrollInterval)
                }
        } else if ("scrollDown" == this.options.trigger) {
            var i = jQuery(window).scrollTop(),
                o = jQuery(document).height();
            t = this;
            0 < o && (this.interval = setInterval(function () {
                var e = jQuery(window).scrollTop() - i;
                e < 0 && (e = 0, i = jQuery(window).scrollTop()), parseFloat(e) / parseFloat(o) > parseFloat(t.options.percentDown) / 100 && (clearInterval(t.interval), t.interval = null, t.complete || (t.callback(), t.complete = !0))
            }, this.options.scrollInterval))
        } else if ("scrollUp" == this.options.trigger) {
            i = jQuery(window).scrollTop(), o = jQuery(document).height(), t = this;
            0 < o && (this.interval = setInterval(function () {
                var e = i - jQuery(window).scrollTop();
                e < 0 && (e = 0, i = jQuery(window).scrollTop()), parseFloat(e) / parseFloat(o) > parseFloat(t.options.percentUp) / 100 && (clearInterval(t.interval), t.interval = null, t.complete || (t.callback(), t.complete = !0))
            }, this.options.scrollInterval))
        } else "timeout" == this.options.trigger && (this.timer = setTimeout(this.callback, this.options.timeout))
    }, this.cancel = function () {
        null !== this.timer && (clearTimeout(this.timer), this.timer = null), null !== this.interval && (clearInterval(this.interval), this.interval = null), this.complete = !0
    }, this.init()
}! function (o) {
    var s = "spin2win_closed",
        e = "spin2win_first_visit",
        n = "";
    if (readCookie(s)) return;
    if (void 0 !== getUrlVars().utm_source) return;
    var t = window.location.hash;
    if (void 0 !== t && t.match("^#p-[0-9]")) return;
    void 0 === window.couponwheel_AnimFrame && (window.couponwheel_AnimFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function () {
        alert("Please upgrade your browser")
    }), couponwheel_notice = function () {
        this.interval = 0, this.reload = function () {
            if (ls = localStorage.getItem("couponwheel_notice"), null !== ls && 0 !== ls.length) return o("#couponwheel_notice").empty(), void o("#couponwheel_notice").append(ls)
        }
    }, window.couponwheel_notice = new couponwheel_notice;
    var h = new function (e) {
        this.wheel_hash = e.wheel_hash, this.wheel_dom = e.wheel_dom, this.timed_trigger = e.timed_trigger, this.exit_trigger = e.exit_trigger, this.show_popup_after = e.show_popup_after, this.preview_key = e.preview_key, this.recaptcha_sitekey = e.recaptcha_sitekey, this.require_recaptcha = e.require_recaptcha, this.prevent_triggers_on_mobile = e.prevent_triggers_on_mobile, this.kiosk_mode = e.kiosk_mode, this.confirm_close_text = e.confirm_close_text, this.on_win_url = "", this.recaptcha_rendered = !1, this.exit_triggered = !1, this.can_show = !0, this.can_close = !0, this.reload_page = !1, this.window_scroll_value = 0, this.render_recaptcha = function () {
            grecaptcha.render("couponwheel" + this.wheel_hash + "_recaptcha", {
                sitekey: this.recaptcha_sitekey
            }), this.recaptcha_rendered = !0
        }, this.ios_input_workaround_enable = function () {
            !1 !== this.is_ios() && (this.window_scroll_value = o(window).scrollTop(), o(window).scrollTop(0), o(".couponwheel_popup").css({
                position: "absolute",
                top: 0
            }), o("body").addClass("couponwheel_ios_stop_scrolling"))
        }, this.ios_input_workaround_disable = function () {
            !1 !== this.is_ios() && (o("body").removeClass("couponwheel_ios_stop_scrolling"), o(window).scrollTop(this.window_scroll_value))
        }, this.is_ios = function () {
            return !!/iPad|iPhone|iPod/.test(navigator.userAgent)
        }, this.is_embed = function () {
            return -1 !== o(this.wheel_dom).parent()[0].className.indexOf("couponwheel_embed")
        }, this.show_popup = function (e) {
            void 0 === e && (e = !0), !1 !== this.can_show && (e && this.exit_triggered || (this.exit_triggered = !0, this.can_show = !1, this.require_recaptcha && !1 === this.recaptcha_rendered && this.render_recaptcha(), this.is_embed() || this.ios_input_workaround_enable(), o(this.wheel_dom).css("pointer-events", "auto"), o(this.wheel_dom + " .couponwheel_popup").show(), this.is_embed() || (o(this.wheel_dom + " .couponwheel_popup_shadow").show(), o(this.wheel_dom + " .couponwheel_popup").css({
                left: "-100%"
            }), o(this.wheel_dom + " .couponwheel_popup").animate({
                left: "0px"
            }, 500, "easeOutExpo"))))
        }, this.close_popup = function () {
            !1 !== this.can_close && (this.can_close = !1, o(this.wheel_dom).css("pointer-events", "none"), o(this.wheel_dom + " .couponwheel_popup").css({
                left: "0%"
            }), o(this.wheel_dom + " .couponwheel_popup").animate({
                left: "-100%"
            }, 600, "easeInExpo", function () {
                o(this.wheel_dom + " .couponwheel_popup_shadow").hide(), o(this.wheel_dom + " .couponwheel_popup").hide(), this.can_close = !0, this.can_show = !0, this.kiosk_mode ? this.reset_popup() : (this.reload_page && location.reload(), this.ios_input_workaround_disable(), window.couponwheel_notice.reload())
            }.bind(this)))
        }, this.reset_popup = function () {
            var e = this.is_embed();
            o(this.wheel_dom).remove(), window["couponwheel" + this.wheel_hash] = void 0, window.couponwheel_manual_trigger(this.wheel_hash, e)
        }, this.hide_popup = function () {
            o(this.wheel_dom + " .couponwheel_popup_shadow").hide(), o(this.wheel_dom + " .couponwheel_popup").hide()
        }, this.go_to_stage2 = function () {
            o(this.wheel_dom + " .couponwheel_popup").animate({
                scrollTop: o(this.wheel_dom + " .couponwheel_form_stage2").offset().top
            }, 650, "swing"), o(this.wheel_dom + " .couponwheel_form_stage1").hide(), o(this.wheel_dom + " .couponwheel_form_stage2").removeClass("couponwheel_hidden"), o(this.wheel_dom + " .couponwheel_form_stage2").addClass("animated bounceIn"), this.can_close = !0;
            var e = this;
            setTimeout(function () {
                !1 !== spin2win_redirect ? window.location.href = spin2win_redirect : e.close_popup()
            }, 2e3)
        }, this.submit_form_done = function (e) {
            if (o(this.wheel_dom + " .couponwheel_ajax_loader").hide(), !0 !== e.hide_popup) return "form_error" == e.error_code ? (this.can_close = !0, o(this.wheel_dom + " .couponwheel_popup_form_error_text").html(e.error_msg), void o(this.wheel_dom + " .couponwheel_form_stage1 *").attr("disabled", !1)) : e.success ? (o(this.wheel_dom + " .couponwheel_popup").animate({
                scrollTop: o(this.wheel_dom + " .couponwheel_wheel_crop").offset().top
            }, 650, "swing"), o(this.wheel_dom + " .couponwheel_form_stage2 .couponwheel_popup_heading_text").html(e.stage2_heading_text), o(this.wheel_dom + " .couponwheel_form_stage2 .couponwheel_popup_main_text").html(e.stage2_main_text), this.start_wheel_animation(e.wheel_deg_end, e.wheel_time_end), this.reload_page = e.reload_page, !1 !== e.notice && localStorage.setItem("couponwheel_notice", e.notice), void 0 !== e.on_win_url && (this.on_win_url = e.on_win_url), !1 !== spin2win_convertkit && o.ajax({
                type: "POST",
                url: admin_path + "admin-ajax.php",
                data: {
                    action: "np_spin2win_ajax",
                    email: n
                },
                success: function (e) {
                    console.log(e)
                }
            }), createCookie(s, "1", "365"), void o(".spin2win-s").hide()) : void(this.can_close = !0);
            this.hide_popup()
        }, this.submit_form = function (e) {
            o(".couponwheel_form_stage1 .couponwheel_popup_main_text").text("Please wait..."), o(".couponwheel_form_stage1 input").fadeOut(), o(".couponwheel_form_stage1 button").fadeOut(), o(".couponwheel_form_stage1 p").fadeOut();
            var t = {
                wheel_deg_end: spin2win_stop,
                wheel_time_end: 7500,
                on_win_url: "",
                stage2_heading_text: spin2win_title,
                stage2_main_text: spin2win_text,
                success: !0,
                notice: !1
            };
            this.submit_form_done(t)
        }, this.start_wheel_animation = function (e, t) {
            this.wheel_deg_end = e, this.wheel_time_end = t, this.wheel_time = 0, this.wheel_deg = 0, this.animation_start_time = null, couponwheel_AnimFrame(this.animate.bind(this))
        }, this.wheel_time = 0, this.wheel_deg = 0, this.wheel_deg_end = 0, this.wheel_time_end = 0, this.wheel_deg_ease = 0, this.animation_start_time = null, this.wheel_ease = function (e) {
            return 1 - Math.pow(1 - e, 5)
        }, this.marker_ease = function (e) {
            var t = 1 - Math.pow(1 - 2 * e, 2);
            return t < 0 && (t = 0), t
        }, this.animate = function (e) {
            for (this.animation_start_time || (this.animation_start_time = e), this.wheel_time = e - this.animation_start_time, this.wheel_time > this.wheel_time_end && (this.wheel_time = this.wheel_time_end), this.wheel_deg_ease = this.wheel_ease(this.wheel_deg_end / this.wheel_time_end * this.wheel_time / this.wheel_deg_end), this.wheel_deg = this.wheel_deg_ease * this.wheel_deg_end, .99 < this.wheel_deg_ease && o(this.wheel_dom + " .couponwheel_marker").css({
                    transform: "translateY(-50%) rotate3d(0,0,1,0deg)",
                    "-webkit-transform": "translateY(-50%) rotate3d(0,0,1,0deg)"
                }), ticker_calc = this.wheel_deg - 360 * Math.floor(this.wheel_deg / 360), i = 1; i <= 12; i++) ticker_calc >= 30 * i - 18 && ticker_calc <= 30 * i && (aa = .2, this.wheel_deg_ease > aa && (aa = this.wheel_deg_ease), bb = this.marker_ease(-(30 * i - 18 - ticker_calc) / 10) * (30 * aa), o(this.wheel_dom + " .couponwheel_marker").css({
                transform: "translateY(-50%)  rotate3d(0,0,1," + (0 - bb) + "deg)",
                "-webkit-transform": "translateY(-50%)  rotate3d(0,0,1," + (0 - bb) + "deg)"
            }));
            o(this.wheel_dom + " .couponwheel_wheel").css({
                transform: "rotate3d(0,0,1," + this.wheel_deg + "deg)",
                "-webkit-transform": "rotate3d(0,0,1," + this.wheel_deg + "deg)"
            }), e - this.animation_start_time > this.wheel_time_end ? this.go_to_stage2() : couponwheel_AnimFrame(this.animate.bind(this))
        }, o(this.wheel_dom + " .couponwheel_stage1_submit_btn").attr("disabled", !1), o(this.wheel_dom + " .couponwheel_stage2_continue_btn").click(function () {
            0 < this.on_win_url.length ? window.location = this.on_win_url : this.close_popup()
        }.bind(this)), o(this.wheel_dom + " .couponwheel_spin_again_btn").click(function () {
            this.kiosk_mode = !0, this.close_popup()
        }.bind(this)), o(this.wheel_dom + " .couponwheel_popup_close_btn").click(function () {
            this.close_popup()
        }.bind(this)), o(this.wheel_dom + " .couponwheel_form_stage1").on("submit", function (e) {
            if (n = o('.couponwheel_popup input[type="email"]').val(), console.log("email = " + n), e.preventDefault(), !o("#spin2win-agree-1").is(":checked")) return o(".couponwheel_form .error").css("display", "block"), !1;
            this.can_close = !1, this.submit_form(o(this.wheel_dom + " .couponwheel_form_stage1").serialize())
        }.bind(this));
        var t = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        void 0 === this.prevent_triggers_on_mobile && (this.prevent_triggers_on_mobile = !1), !1 === (t && this.prevent_triggers_on_mobile) && (this.timed_trigger && new couponwheel_DialogTrigger(function () {
            this.show_popup()
        }.bind(this), {
            trigger: "timeout",
            timeout: 1e3 * this.show_popup_after
        }), this.exit_trigger && (new couponwheel_DialogTrigger(function () {
            this.show_popup()
        }.bind(this), {
            trigger: "exitIntent"
        }), new couponwheel_DialogTrigger(function () {
            this.show_popup()
        }.bind(this), {
            trigger: "scrollUp",
            percent: 10,
            scrollInterval: 150
        })))
    }({
        wheel_hash: "50a52f",
        wheel_dom: "#couponwheel50a52f",
        timed_trigger: !1,
        exit_trigger: !1,
        show_popup_after: 0,
        preview_key: !1,
        recaptcha_sitekey: "",
        require_recaptcha: 0,
        prevent_triggers_on_mobile: !1,
        kiosk_mode: !1,
        confirm_close_text: "Close"
    });
    o(".spin2win-s").show(), o(".spin2win-s .close").click(function () {
        o(".spin2win-s").hide(), createCookie(s, "1", "365")
    }), o(".spin2win-s").click(function (e) {
        if (o(e.target).is(".spin2win-s .close")) return !1;
        h.show_popup(0)
    }), readCookie(e) || (createCookie(e, "1", "365"), h.show_popup(0))
}(jQuery);