var htmlElement = document.documentElement;
var bodyElement = document.body;

var MOMO = {
    // _API: "https://publicapi.vienthonga.vn",
    // _URL: "https://vienthonga.vn"
};

function getAll(selector) {
    return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}


var Layout = (function () {


    var scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 100,
        scrollBody = document.querySelector('.page-content'),

        htmlElement = document.querySelector('html');

    var setUserAgent = function () {

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            bodyElement.classList.add("window-phone");
            return;
        }

        if (/android/i.test(userAgent)) {
            bodyElement.classList.add("android");
            return;
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            bodyElement.classList.add("ios");
            return;
        }
    };

    var viewportHeight = function () {
        // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
        let vh = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vh}px`);


        // We listen to the resize event
        window.addEventListener('resize', () => {
            // We execute the same script as before
            let vh = window.innerHeight * 0.01;
            document.documentElement.style.setProperty('--vh', `${vh}px`);
        });


    };


    var scrollToTop = function () {
        var scrollTop = $("#scrollToTop");

        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 200) {
                scrollTop.addClass("active");
            } else {
                scrollTop.removeClass("active");
            }
        });

        scrollTop.click(function () {
            $("body, html").animate({
                    scrollTop: 0
                },
                500
            );
        });
    };


    var handleSlider = function () {

        var bannerSwiper = new Swiper('.banner-slider', {
            loop: false,
            // Disable preloading of all images
            preloadImages: false,
            // Enable lazy loading
            lazy: true,
            slidesPerView: 1,
            autoplay: {
                delay: 5000,
            },

            pagination: {
                el: '.swiper-pagination',
            }
        });


        var htuSwiper = new Swiper('.htu-slider', {
            loop: false,
            // Disable preloading of all images
            preloadImages: false,
            // Enable lazy loading
            lazy: {
                loadPrevNext: true,
            },
            slidesPerView: 1,


            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
        });

        $('.htu-modal').on('shown.bs.modal', function () {

            var mySwiper = this.querySelector('.swiper-container').swiper

            mySwiper.update();

            // $(this).find(".htu-slider").swiper().update();
        });


        var bannerSwiper = new Swiper('.single-slider', {
            loop: false,
            // Disable preloading of all images
            preloadImages: false,
            // Enable lazy loading
            lazy: {
                loadPrevNext: true,
            },
            slidesPerView: 1,


            pagination: {
                el: '.swiper-pagination',
            }
        });


    };



    var scrollDown = function () {
        scrollBody.addEventListener('scroll', function () {

            // var currentTop = $(window).scrollTop();
            var currentTop = scrollBody.scrollTop;

            if (previousTop - currentTop > scrollDelta || currentTop < 30) {
                //if scrolling up...
                htmlElement.classList.remove('is-scrolldown');
            } else if (currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
                //if scrolling down...
                htmlElement.classList.add('is-scrolldown');
            }

            previousTop = currentTop;
            scrolling = false;
        });


    };




    var handleModal = function () {
        var rootEl = document.documentElement;
        var $modals = getAll('.popup');
        var $modalButtons = getAll('.popup-button');
        var $modalCloses = getAll('.popup-close, .popup-overlay');

        if ($modalButtons.length > 0) {
            $modalButtons.forEach(function ($el) {
                $el.addEventListener('click', function () {
                    var target = $el.dataset.target;
                    openModal(target);
                });
            });
        }

        if ($modalCloses.length > 0) {
            $modalCloses.forEach(function ($el) {
                $el.addEventListener('click', function () {
                    closeModals();
                });
            });
        }

        function openModal(target) {
            var $target = document.getElementById(target);
            rootEl.classList.add('modal-open');
            $target.classList.add('is-active');
        }

        function closeModals() {
            rootEl.classList.remove('modal-open');
            $modals.forEach(function ($el) {
                $el.classList.remove('is-active');
            });
        }

        document.addEventListener('keydown', function (event) {
            var e = event || window.event;
            if (e.keyCode === 27) {
                closeModals();
                closeDropdowns();
            }
        });

    };

    var linkScroll = function () {
        $("a[data-scrollto]").click(function () {
            var element = $(this),
                divScrollToAnchor = element.attr("data-scrollto"),
                divScrollSpeed = element.attr("data-speed"),
                divScrollOffset = element.attr("data-offset");

            // if (element.parents("#primary-menu").hasClass("on-click")) {
            //   return true;
            // }

            if (!divScrollSpeed) {
                divScrollSpeed = 750;
            }
            if (!divScrollOffset) {
                divScrollOffset = 0;
            }

            $("html,body")
                .stop(true)
                .animate({
                        scrollTop: $(divScrollToAnchor).offset().top - Number(divScrollOffset)
                    },
                    Number(divScrollSpeed)
                );

            return false;
        });
    };


    return {
        init: function () {
            setUserAgent();
            scrollToTop();
            handleSlider();
            // handleModal();
            scrollDown();
            linkScroll();
        },
        sliderUpdate: function () {
            handleSliderUpdate()
        }
    };
})();





$(document).ready(function () {

    document.body.classList.add("page-load");


    
    Layout.init();


    if ($('.popup-gallery')) {
        $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                }
            }
        });
    }


});

/*
var scrollBody1 =  document.querySelector('.page-content');

function myFunction() {
    var winScroll = scrollBody1.scrollTop;
    console.log(winScroll);
    console.log("dwd");
    var height = scrollBody1.scrollHeight - scrollBody1.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("myBar").style.width = scrolled + "%";
}

scrollBody1.addEventListener('scroll', function(){
    myFunction()
})
*/